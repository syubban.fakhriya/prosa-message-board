# Message Board (Anomo) - Alpha

## About The Project
Anomo is a platform where people can share their interests, hobbies and passions and connect each other.

<p align="center">
  <img src="documentation_assets/apps.gif" alt="Animo" />s
</p>

## Current Development
For alpha version, this apps use mock api server (mockoon) to mimicking database response

## Mockup & Designs
The design is available in [Figma](https://www.figma.com/file/KNsUf9jjOOwvRry8q5ppIB/Prosa-Message-Board?node-id=0%3A1) 

## Library, Tools, Framework being Used
1. [React Native (Expo Managed Workflow)](https://expo.dev/) (Framework) - For Build Native Apps
2. [React Navigation](https://reactnavigation.org/) (Library) - Routing Purpose
3. [React Redux](https://redux.js.org/) (Library) - State Management
4. [React SWR](https://swr.vercel.app/) (Library) - Improve Data Fetching
5. [Async Storage](https://github.com/react-native-async-storage/async-storage) (Library) - Persist Data
6. [Masked View](https://github.com/react-native-masked-view/masked-view) (Library) - Create Skeleton Loader
7. [Axios](https://github.com/axios/axios) (Library) - Improved HTTP Client
8. [Moment](https://momentjs.com/) (Library) - Manipulate Date and Time
9. [Redux Thunk](https://github.com/reduxjs/redux-thunk) (Library) - Redux Middleware to Simplify Reducer Code
10. [Typescript](https://www.typescriptlang.org/) (Language) - Javascript Abstraction
11. [Mockoon]() (Tools) - Mock API Response Locally

## Feature List
1. Login
2. Register 
3. Home (Contains all post from registered user)
4. Landing (Contains registerd users)
5. Landing User Home (Contains Posts from selected registerd user)
6. Create Post
7. Profile
8. Chat with fellow user (currently unavailable)

## Getting Started

### Prerequisities
1. Npm installed

Download and install nodejs in [https://nodejs.org/en/download/](https://nodejs.org/en/download/), then check the installation using 

```
node -v
npm -v
```

2. Yarn installed

How to install (using npm)
```
npm install --global yarn
```

3. Expo installed
How to install (using npm)
```
npm install --global expo-cli
```

4. Android Studio or Xcode installed

- For Android Studio [https://developer.android.com/studio](https://developer.android.com/studio)
- For Xcode [https://developer.apple.com/xcode/](https://developer.apple.com/xcode/)

5. Mockoon installed

Download and install mockoon in [https://mockoon.com/](https://mockoon.com/)

### Instalation
1. Clone the repository
```
git clone https://gitlab.com/syubban.fakhriya/prosa-message-board.git
cd prosa-message-board
```
2. Install the package manager (yarn)
```
yarn init
``` 

## Usage
Run the following command in terminal
```
expo start
```

After the metro server started, you can choose following option 
```
1. These two option required emulator or simulator opened first
Press a | open Android
Press i | open iOS simulator

Press w | open web
Press r | reload app
Press m | toggle menu
Press d | show developer tools
```

Import prosa_mock_v2.json in mockoon and then start the server like below

![Mockoon](documentation_assets/mockoon.gif)

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.