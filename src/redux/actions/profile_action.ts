import { Profile } from "../../feature/landing/models/profile";

export interface GetProfileAction {
  readonly type: 'GET_PROFILE_ACTION',
  payload: Profile
}

export type ProfileAction = GetProfileAction;