import { Profile } from "../../feature/landing/models/profile";

export interface GetListUserAction {
  readonly type: 'GET_LIST_USER_ACTION',
  payload: Profile[]
}

export type LandingAction = GetListUserAction;