import { Auth, AuthState } from "../../feature/auth/models/auth";

export interface LoginAction {
  readonly type: 'LOGIN_ACTION',
  payload: Auth
}

export interface AuthStateAction {
  readonly type: 'AUTH_STATE_ACTION',
  payload: AuthState
}

export type AuthAction = LoginAction | AuthStateAction;