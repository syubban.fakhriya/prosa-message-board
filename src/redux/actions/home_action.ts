import { Post } from "../../feature/home/models/home";

export interface GetListPostAction {
  readonly type: 'GET_LIST_POST_ACTION',
  payload: Post[]
}

export interface GetPostAction {
  readonly type: 'GET_POST_ACTION',
  payload: Post
}

export type HomeAction = GetListPostAction | GetPostAction;