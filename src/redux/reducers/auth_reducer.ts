import { Auth, AuthState } from "../../feature/auth/models/auth";
import { AuthAction } from "../actions/auth_action";

interface AuthReducerState {
  auth?: Auth,
  authState?: AuthState
}

const initialState: AuthReducerState = {

}

const AuthReducer = (state: AuthReducerState = initialState, action: AuthAction) => {
  switch (action.type) {
    case 'LOGIN_ACTION':
      return {
        ...state,
        auth: action.payload,
      }
    case 'AUTH_STATE_ACTION':
      return {
        ...state,
        authState: action.payload
      }
    default:
      return state;
  }
};

export { AuthReducer };