import { Profile } from "../../feature/landing/models/profile";
import { LandingAction } from "../actions/landing_action"


interface LandingState {
  users?: Profile[],
}

const initialState: LandingState = {
  users: [
    {
      uid: "1",
      email: "irene.gonzalez@example.com",
      name: "Irene Gonzales",
      profile_picture: "https://randomuser.me/api/portraits/women/39.jpg",
      username: "lazyladybug738",
    },
    {
      uid: "2",
      email: "marta.samuels@example.com",
      name: "Marta Samuels",
      profile_picture: "https://randomuser.me/api/portraits/women/25.jpg",
      username: "ticklishlion763",
    },
    {
      uid: "3",
      email: "rosie.davidson@example.com",
      name: "Rosie Davidson",
      profile_picture: "https://randomuser.me/api/portraits/women/84.jpg",
      username: "angryelephant796",
    },
    {
      uid: "4",
      email: "ruth.odden@example.com",
      name: "Ruth Odden",
      profile_picture: "https://randomuser.me/api/portraits/women/63.jpg",
      username: "purplemouse405",
    },
    {
      uid: "5",
      email: "barb.lee@example.com",
      name: "Barb Lee",
      profile_picture: "https://randomuser.me/api/portraits/women/34.jpg",
      username: "smallwolf985",
    },
  ],
}

const LandingReducer = (state: LandingState = initialState, action: LandingAction) => {
  switch (action.type) {
    case 'GET_LIST_USER_ACTION':
      return {
        ...state,
      }
    default:
      return state;
  }
}

export { LandingReducer };