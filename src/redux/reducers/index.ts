import { combineReducers } from "redux";
import { AuthReducer } from "./auth_reducer";
import { HomeReducer } from "./home_reducer";
import { LandingReducer } from "./landing_reducer";
import { ProfileReducer } from "./profile_reducer";

import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistReducer } from 'redux-persist';

const persistConfig = {
  key: 'token',
  storage: AsyncStorage,
  whitelist: ['auth']
};

const rootReducer = combineReducers({
  homeReducer: HomeReducer,
  landingReducer: LandingReducer,
  profileReducer: ProfileReducer,
  //@ts-ignore
  authReducer: persistReducer(persistConfig, AuthReducer),
});

export type ApplicationState = ReturnType<typeof rootReducer>;

export { rootReducer };