import { Post } from "../../feature/home/models/home";
import { HomeAction } from "../actions";


interface PostState {
  posts?: Post[],
  post?: Post,
}

const initialState: PostState = {};

const HomeReducer = (state: PostState = initialState, action: HomeAction) => {
  switch (action.type) {
    // Get Post List
    case 'GET_LIST_POST_ACTION':
      return {
        ...state,
      }
    case 'GET_POST_ACTION':
      return {
        ...state,
        post: action.payload,
      }
    default:
      return state;
  }
};

export { HomeReducer };
