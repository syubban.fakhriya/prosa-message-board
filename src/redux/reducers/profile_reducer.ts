import { Profile } from "../../feature/landing/models/profile";
import { ProfileAction } from "../actions/profile_action";

interface ProfileState {
  user?: Profile,
}

const initialState: ProfileState = {
  user: {
    uid: "1",
    email: "irene.gonzalez@example.com",
    name: "Irene Gonzales",
    profile_picture: "https://randomuser.me/api/portraits/women/39.jpg",
    username: "lazyladybug738",
  }
}

const ProfileReducer = (state: ProfileState = initialState, action: ProfileAction) => {
  switch (action.type) {
    case 'GET_PROFILE_ACTION':
      return {
        ...state
      }
    default:
      return state
  }
}

export { ProfileReducer }