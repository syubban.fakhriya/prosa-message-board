import React from "react";
import { SafeAreaView, View, Text, FlatList } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { Avatar } from "react-native-paper";
import { connect } from "react-redux";
import RouteNames from "../../../navigation/route_names";
import { CustomColors } from "../../../utils/colors";
import CustomAppBar from "../../../utils/components/appbar";
import { GlobalProps, mapStateToProps } from "../../../utils/props";
import { onLogout, useProfile } from "../presenter/profile_presenter";

interface state {
  onLogout: Function
}

const ProfilePage: React.FC<GlobalProps & state> = (props) => {
  const { auth, authState } = props.authReducer;
  const buttonList: Record<string, any>[] = [
    {
      "title": "Edit Profile",
      "onPress": () => { }
    },
    {
      "title": "Privacy Policy",
      "onPress": () => { }
    },
    {
      "title": "Logout",
      "onPress": async () => {
        !authState?.isLoading ? props.onLogout(props) : () => { }
      }
    }
  ];

  const [refreshing, setRefreshing] = React.useState(false);
  const { profile, isError, isLoading, mutate } = useProfile(auth?.access_token ?? "");

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    mutate().then((response) => setRefreshing(false)).catch((error) => setRefreshing(false))
  }, []);

  const result = profile?.result;

  return <>
    <SafeAreaView style={{ display: "flex", flex: 1, flexGrow: 1 }}>
      <CustomAppBar title="Profile" />
      <View style={{ flex: 1, flexDirection: 'column' }}>
        <View style={{ flexDirection: "row", alignItems: "center", backgroundColor: CustomColors.water, paddingHorizontal: 16, paddingVertical: 16 }}>
          <Avatar.Image size={60} source={{ uri: result?.profile_picture }} style={{ marginRight: 16 }} />
          <View>
            <Text style={{ fontSize: 16, marginBottom: 6, fontWeight: '500' }}>{result?.name}</Text>
            <Text style={{ fontSize: 14 }}>{result?.username}</Text>
          </View>
        </View>
        <View style={{ height: 6, backgroundColor: CustomColors.gainsboro }}></View>
        <FlatList
          data={buttonList}
          renderItem={({ item }) => <TouchableOpacity onPress={() => item["onPress"]()}>
            <View style={{ paddingHorizontal: 16, paddingVertical: 24 }}>
              <Text style={{ fontSize: 20, fontWeight: '400' }}>{item["title"]}</Text>
            </View>
          </TouchableOpacity>
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={{ backgroundColor: CustomColors.gainsboro, height: 3 }}></View>}
        />
      </View>
    </SafeAreaView>
  </>
}

export default connect(mapStateToProps, { onLogout })(ProfilePage);