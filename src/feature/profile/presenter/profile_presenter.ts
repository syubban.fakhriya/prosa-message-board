import { Dispatch } from "redux"
import RouteNames from "../../../navigation/route_names"
import { AuthAction } from "../../../redux/actions/auth_action"
import { GlobalProps } from "../../../utils/props"
import AsyncStorage from '@react-native-async-storage/async-storage';
import { BASE_URL, ERROR, LOADING, SUCCESS } from "../../../utils/constant/constant";
import useSWR from "swr";
import { BaseResponse } from "../../../models/base_response";
import { Profile } from "../../landing/models/profile";
import axios from "axios";

const onLogout = (props: GlobalProps) => {
  return async (dispatch: Dispatch<AuthAction>) => {
    dispatch({
      type: 'AUTH_STATE_ACTION',
      payload: {
        isLoading: true,
        isError: false,
        message: LOADING
      }
    })

    AsyncStorage.removeItem('persist:token').then((result) => {
      dispatch({
        type: 'AUTH_STATE_ACTION',
        payload: {
          isLoading: false,
          isError: false,
          message: SUCCESS
        }
      })

      props.navigation.reset({ index: 0, routes: [{ name: RouteNames.LOGIN }] })
    }).catch((error: Error) => {
      dispatch({
        type: 'AUTH_STATE_ACTION',
        payload: {
          isLoading: false,
          isError: false,
          message: error.message
        }
      })

      console.log(error.message)
    })
  }
}

const fetcher = (url: string, token: string) => axios.get(url, {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  }
}).then(r => r.data)

const useProfile = (token: string) => {
  const { data, error, mutate } = useSWR<BaseResponse<Profile>>(`${BASE_URL}/profile`, (url) => fetcher(url, token));

  return {
    profile: data,
    isLoading: !error && !data,
    isError: error,
    mutate: mutate,
  }
}

export { onLogout, useProfile }