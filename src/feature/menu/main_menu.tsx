import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import React from "react";
import { MainMenuRoutes } from "../../navigation/routes";
import RouteNames from "../../navigation/route_names";
import { FontAwesome5, AntDesign, Ionicons } from '@expo/vector-icons';
import { GlobalProps, mapStateToProps } from "../../utils/props";
import { CustomColors } from "../../utils/colors";
import { connect } from "react-redux";

const Tab = createMaterialBottomTabNavigator();

const MainMenu: React.FC<GlobalProps> = (props?) => {
  const generateScreens = () => {
    const screens: any[] = [];

    let i = 0;
    for (let screenName in MainMenuRoutes) {
      // @ts-ignore
      const screen = MainMenuRoutes[screenName];
      screens.push(<Tab.Screen
        key={i}
        name={screen.name}
        component={screen.component}
      />)
    }

    return screens;
  }

  return <>
    <Tab.Navigator
      initialRouteName={RouteNames.HOME}
      screenOptions={
        ({ route }) => ({
          tabBarIcon: ({ focused, color }) => {
            switch (route.name) {
              case MainMenuRoutes.HOME.name:
                return <FontAwesome5 name="home" size={20} color={focused ? CustomColors.space_cadet : color} />;
              case MainMenuRoutes.LANDING.name:
                return <AntDesign name="profile" size={20} color={focused ? CustomColors.space_cadet : color} />;
              case MainMenuRoutes.PROFILE.name:
                return <Ionicons name="person" size={20} color={focused ? CustomColors.space_cadet : color} />;
              default:
                return <FontAwesome5 name="home" size={20} color={focused ? CustomColors.space_cadet : color} />;
            }
          },
        })
      }
      barStyle={{ backgroundColor: '#fff', shadowOffset: { width: 1, height: 1 }, }}
    >
      {generateScreens()}
    </Tab.Navigator >
  </>
}

export default connect(mapStateToProps)(MainMenu);