import React, { useState } from "react";
import { Modal, Portal, Provider, TextInput, Button, List } from "react-native-paper";
import { Keyboard, ScrollView, Text, TouchableWithoutFeedback } from "react-native";

interface state {
  visible: boolean;
  onDismiss: (isVisible: boolean) => void;
}

const AddPost = ({ visible, onDismiss }: state): React.ReactElement => {
  const [title, setTitle] = useState("");
  const [post, setPost] = useState("");
  const [expanded, setExpanded] = React.useState(true);

  const handlePress = () => setExpanded(!expanded);

  const containerStyle = { backgroundColor: 'white' };

  return <Portal>
    <Modal visible={visible} onDismiss={() => onDismiss(false)} contentContainerStyle={containerStyle} style={{ margin: 24, zIndex: 99 }}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss} accessible={false}>
        <ScrollView
          style={{ margin: 20 }}
          showsVerticalScrollIndicator={false}
          bounces={false}>
          <Text style={{
            marginBottom: 24,
            fontSize: 20,
            fontWeight: '500'
          }}>Create Post</Text>

          <Text style={{
            marginBottom: 8,
            fontSize: 16,
            fontWeight: '500'
          }}>Category</Text>
          <List.Section>
            <List.Accordion
              title="Category List"

              left={props => <List.Icon {...props} icon="folder" />}>
              <List.Item title="Pekerjaan" />
              <List.Item title="Startup" />
              <List.Item title="Corporate" />
            </List.Accordion>
          </List.Section>

          <Text style={{
            marginBottom: 8,
            fontSize: 16,
            fontWeight: '500'
          }}>Title</Text>
          <TextInput
            autoComplete={false}
            value={title}
            dense
            mode={"outlined"}
            style={{
              backgroundColor: 'white',
              marginBottom: 16,
            }}
            onChangeText={(text) => setTitle(text)} />

          <Text style={{
            marginBottom: 8,
            fontSize: 16,
            fontWeight: '500'
          }}>Post</Text>
          <TextInput
            autoComplete={false}
            value={post}
            dense
            style={{
              backgroundColor: 'white',
              marginBottom: 50,
              height: 150,
              paddingTop: 0,
              textAlignVertical: "top",
            }}
            mode={"outlined"}
            multiline
            onChangeText={(text) => setPost(text)} />

          <Button mode='outlined' onPress={() => console.log()} style={{
            padding: 4,
            marginBottom: 16,
            backgroundColor: '#00CEC9',
          }}  >
            <Text style={{
              color: 'white',
              fontWeight: 'bold'
            }}>Post</Text>
          </Button>
        </ScrollView>
      </TouchableWithoutFeedback>
    </Modal>
  </Portal>
}

export default AddPost;