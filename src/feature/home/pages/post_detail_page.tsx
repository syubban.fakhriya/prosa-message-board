import moment from "moment";
import React, { useEffect, useState } from "react";
import { View, Image, Text, FlatList, ScrollView, SafeAreaView, RefreshControl } from "react-native";
import { Chip, TextInput } from "react-native-paper";
import { connect } from "react-redux";
import { CustomColors } from "../../../utils/colors";
import CustomAppBar from "../../../utils/components/appbar";
import { GlobalProps, mapStateToProps } from "../../../utils/props";
import PostResponseComponent from "../components/post_response_component";
import { Ionicons } from '@expo/vector-icons';
import { usePostDetail } from "../presenter/home_presenter";

const PostDetailPage: React.FC<GlobalProps> = (props) => {

  const { auth } = props.authReducer;
  const { post_id } = props.route.params;
  const { post, error, isLoading, mutate } = usePostDetail(auth?.access_token ?? "", post_id);
  const [refreshing, setRefreshing] = React.useState(false);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    mutate().then((response) => setRefreshing(false)).catch((error) => setRefreshing(false))
  }, []);

  const [currentDate, setCurrentDate] = useState("");

  const result = post?.result;

  useEffect(() => {
    setCurrentDate(moment(result?.created_at).fromNow());
  }, [])

  return <SafeAreaView style={{ display: "flex", flexDirection: "column", flexGrow: 1 }}>
    <CustomAppBar title="Post Detail" enableBack={true} backAction={() => props.navigation.goBack()} />

    <ScrollView
      refreshControl={
        <RefreshControl
          refreshing={refreshing}
          onRefresh={onRefresh}
        />
      }
      style={{ flex: 1 }}
      contentContainerStyle={{ paddingVertical: 24 }}>
      <View>
        <View style={{
          paddingHorizontal: 10,
          paddingVertical: 5
        }}>
          <View style={{ marginBottom: 16, display: "flex", flexDirection: "row" }}>
            <Image source={{ uri: result?.author.profile_picture }} style={{ width: 34, height: 34, marginRight: 8, borderRadius: 8 }} />
            <View>
              <Text style={{ fontSize: 12, marginBottom: 4 }}>{result?.author.name}</Text>
              <Text style={{ fontSize: 12 }}>{currentDate}</Text>
            </View>
          </View>
          <Text style={{ fontSize: 18, marginBottom: 10, fontWeight: "500" }}>{result?.title}</Text>
          <FlatList
            horizontal={true}
            data={result?.tags}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => <View style={{ width: 5 }}></View>}
            renderItem={({ item }) =>
              <Chip mode="outlined" textStyle={{ fontSize: 12, color: 'grey' }} >{item.title}</Chip>}
          />

          <Text style={{ fontSize: 13, marginTop: 10, marginBottom: 8 }}>{result?.body}</Text>

          <View style={{ display: "flex", flexDirection: "row", alignSelf: "flex-end", marginBottom: 8 }}>
            <Text style={{ fontSize: 13, color: 'grey' }}>{`${result?.like_count} menyukai`}</Text>
            {((result?.responses?.length ?? 0) > 0) ? <Text style={{ fontSize: 13, marginLeft: 8, color: 'grey' }}>{`${result?.responses?.length} komentar`}</Text> : <></>}
          </View>
        </View>
        <View style={{ height: 6, backgroundColor: CustomColors.gainsboro }}></View>

        <View style={{
          marginHorizontal: 10,
          marginTop: 16,
          marginBottom: 16,
          backgroundColor: CustomColors.ming,
          paddingVertical: 5,
          paddingHorizontal: 10,
          alignSelf: "baseline",
          borderRadius: 10
        }}>
          <Text style={{
            fontSize: 16,
            fontWeight: '500',
            color: 'white'
          }}>Balasan</Text>
        </View>
      </View>

      {
        result?.responses?.map((item, index) => <PostResponseComponent post={item} key={index} />)
      }
    </ScrollView>

    <View style={{
      display: "flex",
      flexDirection: "row",
      alignItems: "center",
      paddingVertical: 10,
      paddingHorizontal: 16,
      borderTopColor: CustomColors.gainsboro,
      borderTopWidth: 1,
    }}>
      <Image source={{ uri: "https://i.picsum.photos/id/42/200/200.jpg?hmac=jc_eDuYgXmIOC_4gl2wEY0jgxC2rMPJbDF6QJdynR7Q" }} style={{ width: 43, height: 43, marginRight: 10, borderRadius: 8, marginTop: 7, borderWidth: .4 }} />
      <TextInput
        dense
        style={{
          backgroundColor: 'white',
          flex: 1,
          marginRight: 10,
        }}
        autoComplete={false}
        label={"Tambah Komentar..."}
        value={""}
        mode={"outlined"}
      />
      <Ionicons name="send" size={36} color={'grey'} style={{
        marginTop: 4
      }} />
    </View>
  </SafeAreaView>

}

export default connect(mapStateToProps, {})(PostDetailPage);