import { FlatList, RefreshControl, SafeAreaView, Text, View } from "react-native";
import { ActivityIndicator, Appbar } from "react-native-paper";
import { connect } from "react-redux";
import { CustomColors } from "../../../utils/colors";
import CustomAppBar from "../../../utils/components/appbar";
import { GlobalProps, mapStateToProps } from "../../../utils/props";
import PostComponent from "../components/post_component";
import { Ionicons, AntDesign } from '@expo/vector-icons';
import RouteNames from "../../../navigation/route_names";
import { onPostDetail, usePostList } from "../presenter/home_presenter";
import AddPost from "./modal/add_post";
import React, { useState } from "react";
import PostSkeleton from "../components/post_skeleton";

interface state {
  onPostDetail: Function;
}

const HomePage: React.FC<GlobalProps & state> = (props) => {
  // const { posts } = props.homeReducer;
  const { auth } = props.authReducer;
  const [visible, setVisible] = useState(false);

  const { posts, error, isLoading, mutate } = usePostList(auth?.access_token ?? "");
  const [refreshing, setRefreshing] = React.useState(false);

  const showModal = () => setVisible(true);
  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    mutate().then((response) => setRefreshing(false)).catch((error) => setRefreshing(false))
  }, []);

  return <>
    <SafeAreaView style={{ display: "flex", flex: 1, flexGrow: 1 }}>

      <View style={{ flex: 1 }}>
        <CustomAppBar title="Home" actions={[
          <Appbar.Action key={'add'} icon={() => <AntDesign name="pluscircle" size={24} color={CustomColors.space_cadet} />} onPress={() => showModal()} />
        ]} />


        {!isLoading ?
          <FlatList
            ItemSeparatorComponent={() => <View style={{ backgroundColor: CustomColors.gainsboro, height: 8, marginVertical: 16 }}></View>}
            data={posts?.result}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
            renderItem={({ item, index }) => {
              return <PostComponent
                post={item} navigation={{
                  navigate: () => {
                    props.onPostDetail(item);
                    props.navigation.navigate(RouteNames.POST_DETAIL, { post_id: item.id });
                  },
                  goBack: () => {
                    props.navigation.goBack();
                  }
                }} />
            }}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              paddingVertical: 24,
            }} /> :
          <FlatList
            data={[0, 0, 0, 0, 0, 0]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item) => <PostSkeleton />}
          />}
      </View>

      <AddPost onDismiss={(isVisible) => setVisible(isVisible)} visible={visible} />
    </SafeAreaView>
  </>
}

export default connect(mapStateToProps, { onPostDetail })(HomePage);