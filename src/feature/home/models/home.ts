import { Profile } from "../../landing/models/profile";

export interface Post {
  id?: number,
  title?: string,
  body?: string,
  like_count?: number,
  created_at?: string,
  tags?: Tag[],
  author: Profile,
  responses?: Post[]
}

export interface Tag {
  id?: number,
  title?: string,
}