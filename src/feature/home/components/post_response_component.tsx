import moment from "moment";
import React, { useEffect, useState } from "react";
import { View, Image, Text } from "react-native";
import { Post } from "../models/home";

interface state {
  post: Post;
}

const PostResponseComponent = ({ post }: state): React.ReactElement => {
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    setCurrentDate(moment(post?.created_at).fromNow());
  }, [])

  return <>
    <View style={{
      paddingHorizontal: 10,
      paddingVertical: 5
    }}>
      <View style={{ marginBottom: 8, display: "flex", flexDirection: "row" }}>
        <Image source={{ uri: post?.author.profile_picture }} style={{ width: 34, height: 34, marginRight: 8, borderRadius: 8 }} />
        <View>
          <Text style={{ fontSize: 12, marginBottom: 4 }}>{post?.author.name}</Text>
          <Text style={{ fontSize: 12 }}>{currentDate}</Text>
        </View>
      </View>

      <Text style={{ fontSize: 13, marginTop: 10, marginBottom: 8 }}>{post?.body}</Text>

    </View>
  </>;
}

export default PostResponseComponent;