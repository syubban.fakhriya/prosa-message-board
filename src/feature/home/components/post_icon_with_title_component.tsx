import React, { ReactNode } from "react";
import { View, Text } from "react-native";

interface state {
  icon: ReactNode,
  title: string,
  color: string,
}

const PostIconWithTitleComponent = ({ icon, title, color }: state): React.ReactElement => {
  return <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
    {icon}
    <Text style={{ marginLeft: 8, color: color }}>{title}</Text>
  </View>
}

export default PostIconWithTitleComponent;