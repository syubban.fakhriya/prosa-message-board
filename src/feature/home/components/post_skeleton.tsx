import React from 'react';
import { View, Text } from 'react-native';
import SkeletonContent from 'react-native-skeleton-content';

const PostSkeleton = () => {
  return <SkeletonContent
    isLoading={true}
    containerStyle={{
      display: 'flex',
      margin: 24
    }}
  >
    <View style={{ width: 34, height: 34, borderRadius: 8, marginBottom: 10 }}></View>
    <View style={{ width: '100%', height: 34, borderRadius: 8, marginBottom: 10 }}></View>
    <View style={{ width: '100%', height: 34, borderRadius: 8, marginBottom: 10 }}></View>
    <View style={{ width: '100%', height: 34, borderRadius: 8 }}></View>
  </SkeletonContent>
}

export default PostSkeleton;