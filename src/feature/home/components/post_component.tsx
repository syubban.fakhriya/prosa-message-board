import { View, Text, Image, FlatList, TouchableOpacity } from "react-native"
import { CustomColors } from "../../../utils/colors";
import { Post } from "../models/home";
import { Ionicons, AntDesign } from '@expo/vector-icons';
import PostIconWithTitleComponent from "./post_icon_with_title_component";
import moment from "moment";
import { useEffect, useState } from "react";
import { Chip } from "react-native-paper";

interface state {
  post: Post,
  navigation: { navigate: Function, goBack: Function }
}

const PostComponent = ({ post, navigation }: state): React.ReactElement => {
  const { author } = post;
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    setCurrentDate(moment(post.created_at).fromNow());
  }, [])

  return <>
    <View style={{
      paddingHorizontal: 10,
      paddingVertical: 5
    }}>

      <View style={{ marginBottom: 16, display: "flex", flexDirection: "row" }}>
        <Image source={{ uri: post.author.profile_picture }} style={{ width: 34, height: 34, marginRight: 8, borderRadius: 8 }} />
        <View>
          <Text style={{ fontSize: 12, marginBottom: 4 }}>{author.name}</Text>
          <Text style={{ fontSize: 12 }}>{currentDate}</Text>
        </View>
      </View>
      <Text style={{ fontSize: 18, marginBottom: 10, fontWeight: "500" }}>{post.title}</Text>
      <FlatList
        horizontal={true}
        data={post.tags}
        ItemSeparatorComponent={() => <View style={{ width: 5 }}></View>}
        renderItem={({ item }) =>
          <Chip mode="outlined" textStyle={{ fontSize: 12, color: 'grey' }} >{item.title}</Chip>}
        keyExtractor={(item, index) => index.toString()}
      />

      <Text style={{ fontSize: 13, marginTop: 10, marginBottom: 8 }}>{post.body}</Text>

      <View style={{ display: "flex", flexDirection: "row", alignSelf: "flex-end" }}>
        <Text style={{ fontSize: 13, color: 'grey' }}>{`${post.like_count} menyukai`}</Text>
        {((post.responses?.length ?? 0) > 0) ? <Text style={{ fontSize: 13, marginLeft: 8, color: 'grey' }}>{`${post.responses?.length} komentar`}</Text> : <></>}
      </View>

      <View style={{ backgroundColor: CustomColors.gainsboro, height: 4, marginVertical: 16 }}></View>
      <View style={{ display: "flex", justifyContent: "space-around", flexDirection: "row" }}>
        <PostIconWithTitleComponent icon={<AntDesign name="like2" size={20} color={CustomColors.space_cadet} />} title={"Like"} color={CustomColors.space_cadet} />
        <TouchableOpacity onPress={() => navigation.navigate()}>
          <PostIconWithTitleComponent icon={<Ionicons name="chatbubbles-outline" size={20} color={CustomColors.space_cadet} />} title={"Komentar"} color={CustomColors.space_cadet} />
        </TouchableOpacity>
      </View>
    </View>
  </>
}

export default PostComponent;