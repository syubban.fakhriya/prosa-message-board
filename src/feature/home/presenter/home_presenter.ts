import { Dispatch } from "redux";
import useSWR from "swr";
import { BaseResponse } from "../../../models/base_response";
import { HomeAction } from "../../../redux/actions";
import { BASE_URL } from "../../../utils/constant/constant";
import { Post } from "../models/home";
import axios from 'axios';

const onPostDetail = (post: Post) => {
  return (dispatch: Dispatch<HomeAction>) => {
    dispatch({
      type: 'GET_POST_ACTION',
      payload: post,
    });
  }
}

const fetcher = (url: string, token: string) => axios.get(url, {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  }
}).then(r => r.data)

const usePostList = (token: string) => {
  const { data, error, mutate } = useSWR<BaseResponse<Post[]>>(`${BASE_URL}/blog`, (url) => fetcher(url, token));

  return {
    posts: data,
    isLoading: !error && !data,
    error: error,
    mutate: mutate
  }
}

const usePostDetail = (token: string, id: string) => {
  const { data, error, mutate } = useSWR<BaseResponse<Post>>(`${BASE_URL}/blog/${id}`, (url) => fetcher(url, token));

  return {
    post: data,
    isLoading: !error && !data,
    error: error,
    mutate: mutate
  }
}

export { onPostDetail, usePostList, usePostDetail };