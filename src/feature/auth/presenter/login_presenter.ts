import { Dispatch } from "react"
import { BaseResponse } from "../../../models/base_response"
import { AuthAction } from "../../../redux/actions/auth_action"
import { BASE_URL, LOADING } from "../../../utils/constant/constant"
import { Auth } from "../models/auth"
import axios, { AxiosError } from 'axios';
import { GlobalProps } from "../../../utils/props"
import RouteNames from "../../../navigation/route_names"

const onLogin = (username: string, password: string, props: GlobalProps) => {
  return async (dispatch: Dispatch<AuthAction>) => {
    dispatch({
      type: 'AUTH_STATE_ACTION',
      payload: {
        isError: false,
        isLoading: true,
        message: LOADING
      }
    })

    axios.post<BaseResponse<Auth>>(`${BASE_URL}/login`,
      {
        "username": username,
        "password": password
      }).then(response => {
        console.log(response.data)

        dispatch({
          type: 'LOGIN_ACTION',
          payload: response.data.result,
        })

        dispatch({
          type: 'AUTH_STATE_ACTION',
          payload: {
            isError: false,
            isLoading: false,
            message: response.data.message
          }
        })

        props.navigation.navigate(RouteNames.MAIN_MENU)
      }).catch((error: Error | AxiosError) => {
        if (axios.isAxiosError(error)) {
          // Access to config, request, and response
          console.log(error.message)
          dispatch({
            type: 'AUTH_STATE_ACTION',
            payload: {
              isError: true,
              isLoading: false,
              message: error.message
            }
          })
        } else {
          // Just a stock error
          console.log(error.message)
          dispatch({
            type: 'AUTH_STATE_ACTION',
            payload: {
              isError: true,
              isLoading: false,
              message: error.message
            }
          })
        }
      })
  }
}

const onModalErrorDismiss = (isError: boolean) => {
  return (dispatch: Dispatch<AuthAction>) => {
    dispatch({
      type: 'AUTH_STATE_ACTION',
      payload: {
        isError: isError,
        isLoading: false,
        message: ""
      }
    })
  }
}

export { onLogin, onModalErrorDismiss }