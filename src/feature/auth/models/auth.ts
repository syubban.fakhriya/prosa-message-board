interface Auth {
  access_token?: string,
  token_type?: string
}

interface AuthState {
  isError: boolean,
  isLoading: boolean,
  message: string
}

export { Auth, AuthState }