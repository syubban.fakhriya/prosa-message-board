import { StyleSheet } from "react-native"

const loginStyles = StyleSheet.create({
  inputarea: {
    flex: 1,
    flexGrow: 1,
    justifyContent: "center",
    marginHorizontal: 24,
  },
  signUpContainer: {
    flexDirection: "row",
    justifyContent: "center",
  }
});

export { loginStyles }