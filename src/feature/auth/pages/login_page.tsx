import React from 'react';
import { View, Text, SafeAreaView, TouchableWithoutFeedback, Keyboard } from 'react-native';
import { ActivityIndicator, Button, Card, Snackbar, TextInput } from 'react-native-paper';
import RouteNames from '../../../navigation/route_names';
import { GlobalProps, mapStateToProps } from '../../../utils/props';
import { CustomColors } from '../../../utils/colors';
import { loginStyles } from '../styles/login';
import { connect } from 'react-redux';
import { onLogin, onModalErrorDismiss } from '../presenter/login_presenter';
import ErrorModal from '../../../utils/components/error_modal';

interface state {
  onLogin: Function;
  onModalErrorDismiss: Function;
}

const LoginPage: React.FC<GlobalProps & state> = (props) => {

  const [username, setUsername] = React.useState('');
  const [password, setPassword] = React.useState('');
  const [isMasked, setIsMasked] = React.useState(true);

  const [isUsernameEmpty, setIsUsernameEmpty] = React.useState(false)
  const [isPasswordEmpty, setIsPasswordEmpty] = React.useState(false)

  const { authState } = props.authReducer;

  const onSubmit = () => {
    if (username == "") {
      setIsUsernameEmpty(true)
    } else {
      setIsUsernameEmpty(false)
    }

    if (password == "") {
      setIsPasswordEmpty(true)
    } else {
      setIsPasswordEmpty(false)
    }

    if (!isUsernameEmpty && !isPasswordEmpty) {
      props.onLogin(username, password, props)
    }
  }

  return <>
    <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      <SafeAreaView style={{ display: "flex", flex: 1, flexGrow: 1 }}>

        <View style={loginStyles.inputarea}>
          <View style={{
            marginBottom: 70
          }}>
            <Text style={{
              fontSize: 27,
              fontWeight: '500'
            }}>The Board</Text>
            <Text style={{
              fontSize: 30,
              fontWeight: '700'
            }}>Q&A Forum</Text>
          </View>

          <Card style={{
            elevation: 5,
            paddingBottom: 10,
            borderRadius: 24
          }}>
            <TextInput
              autoComplete={false}
              label={"Username"}
              value={username}
              autoCapitalize="none"
              error={isUsernameEmpty}
              dense
              style={{
                backgroundColor: 'white',
                marginHorizontal: 20
              }}
              onChangeText={(text) => {
                setUsername(text)
                if (username != "") {
                  setIsUsernameEmpty(false);
                }
              }}
              mode={'flat'} />
          </Card>

          <View style={{ height: 20 }} />

          <Card style={{
            elevation: 5,
            paddingBottom: 10,
            borderRadius: 24
          }}>
            <TextInput
              autoComplete={false}
              label={"Password"}
              value={password}
              autoCapitalize="none"
              error={isPasswordEmpty}
              dense
              style={{
                backgroundColor: 'white',
                marginHorizontal: 20
              }}
              secureTextEntry={isMasked}
              right={<TextInput.Icon name="eye" onPress={() => setIsMasked(!isMasked)} />}
              onChangeText={(text) => {
                setPassword(text)
                if (password != "") {
                  setIsPasswordEmpty(false);
                }
              }}
              mode={'flat'} />
          </Card>

          <View style={{ height: 50 }} />

          <Button mode='contained' onPress={() => {
            !authState?.isLoading ? onSubmit() : () => { }
          }} style={{
            borderRadius: 24,
            elevation: 5,
            padding: 10
          }}  >
            {!authState?.isLoading ?
              <Text style={{
                color: '#ffffff',
                fontWeight: 'bold'
              }}>Log In</Text> :
              <ActivityIndicator animating={true} color={"white"} size={20} />}
          </Button>
        </View>

        <View style={loginStyles.signUpContainer}>
          <Text style={{
            marginEnd: 4,
            fontSize: 16,
          }}>Don't have account yet?</Text>
          <Text style={{
            color: CustomColors.primary,
            fontSize: 16,
            fontWeight: '700'
          }}>Sign Up</Text>
        </View>

        <ErrorModal onDismiss={(value) => { props.onModalErrorDismiss(value) }} visible={authState?.isError ?? false} message={authState?.message ?? ""} />
      </SafeAreaView>
    </TouchableWithoutFeedback>
  </>
}

export default connect(mapStateToProps, { onLogin, onModalErrorDismiss })(LoginPage);