interface Profile {
  id: number,
  name?: string,
  email?: string,
  username?: string,
  profile_picture?: string,
}

export { Profile }