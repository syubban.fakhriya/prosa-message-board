import React, { useEffect } from "react";
import { FlatList, View, Text, LogBox } from "react-native";
import { Avatar, Card } from "react-native-paper";
import { useUserList } from "../presenter/landing_presenter";

interface state {
  access_token: string;
}

const UserList = ({ access_token }: state): React.ReactElement => {
  const { users, isError, isLoading } = useUserList(access_token ?? "");

  return <View style={{
    paddingHorizontal: 16,
    paddingVertical: 24
  }}>
    {
      users?.result.map((item, index) => {
        return <View key={index.toString()} style={{ marginBottom: 16 }}>
          <Card style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
            <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
              <Avatar.Image size={68} source={{ uri: item.profile_picture }} style={{ marginRight: 16 }} />
              <View style={{ display: "flex", flexDirection: "column" }}>
                <Text style={{ marginBottom: 4, fontSize: 16, fontWeight: '500' }}>{`${item.name}`}</Text>
                <Text style={{ marginBottom: 4, fontSize: 14, fontWeight: '400' }}>{item.username}</Text>
                <Text style={{ marginBottom: 4, fontSize: 14, fontWeight: '400' }}>{item.email}</Text>
              </View>
            </View>
          </Card>
        </View>
      })
    }
  </View>
}

export default UserList;