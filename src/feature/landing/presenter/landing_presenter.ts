import axios from "axios"
import { Dispatch } from "redux"
import useSWR from "swr"
import { BaseResponse } from "../../../models/base_response"
import { BASE_URL } from "../../../utils/constant/constant"
import { Post } from "../../home/models/home"
import { Profile } from "../models/profile"

const fetcher = (url: string, token: string) => axios.get(url, {
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${token}`
  }
}).then(r => r.data)

const useUserList = (token: string) => {
  const { data, error, mutate } = useSWR<BaseResponse<Profile[]>>(`${BASE_URL}/users`, (url) => fetcher(url, token));

  return {
    users: data,
    isLoading: !error && !data,
    isError: error,
    mutate: mutate,
  }
}

const useUserPostList = (token: string, userId: number) => {
  const { data, error, mutate } = useSWR<BaseResponse<Post[]>>(`${BASE_URL}/blog/user/${userId}`, (url) => fetcher(url, token));

  return {
    posts: data,
    isLoading: !error && !data,
    isError: error,
    mutate: mutate,
  }
}

export { useUserList, useUserPostList }