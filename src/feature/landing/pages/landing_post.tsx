import React from "react";
import { FlatList, RefreshControl, SafeAreaView, ScrollView, View } from "react-native";
import { Appbar } from "react-native-paper";
import { connect } from "react-redux";
import RouteNames from "../../../navigation/route_names";
import CustomAppBar from "../../../utils/components/appbar";
import { GlobalProps, mapStateToProps } from "../../../utils/props";
import PostComponent from "../../home/components/post_component";
import PostSkeleton from "../../home/components/post_skeleton";
import { useUserPostList } from "../presenter/landing_presenter";
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { CustomColors } from "../../../utils/colors";

const LandingPost: React.FC<GlobalProps> = (props) => {
  const { auth } = props.authReducer;
  const { user_id } = props.route.params;
  const [refreshing, setRefreshing] = React.useState(false);
  const { posts, isError, isLoading, mutate } = useUserPostList(auth?.access_token ?? "", user_id);

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    mutate().then((response) => setRefreshing(false)).catch((error) => setRefreshing(false))
  }, []);


  return <>
    <SafeAreaView style={{ display: "flex", flex: 1, flexGrow: 1 }}>
      <CustomAppBar title="Landing Post" enableBack={true} backAction={() => props.navigation.goBack()} />

      <View style={{ flex: 1 }}>

        {!isLoading ?
          <FlatList
            ItemSeparatorComponent={() => <View style={{ backgroundColor: CustomColors.gainsboro, height: 8, marginVertical: 16 }}></View>}
            data={posts?.result}
            refreshControl={
              <RefreshControl
                refreshing={refreshing}
                onRefresh={onRefresh}
              />
            }
            renderItem={({ item, index }) => {
              return <PostComponent
                post={item} navigation={{
                  navigate: () => {
                    props.navigation.navigate(RouteNames.POST_DETAIL, { post_id: item.id });
                  },
                  goBack: () => {
                    props.navigation.goBack();
                  }
                }} />
            }}
            keyExtractor={(item, index) => index.toString()}
            contentContainerStyle={{
              paddingVertical: 24,
            }} /> :
          <FlatList
            data={[0, 0, 0, 0, 0, 0]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item) => <PostSkeleton />}
          />}
      </View>
    </SafeAreaView>
  </>
}

export default connect(mapStateToProps, {})(LandingPost);