import React, { useEffect, useState } from "react";
import { FlatList, SafeAreaView, View, Text, Image, ScrollView, LogBox, RefreshControl, TouchableOpacity } from "react-native";
import { Avatar, Button, Card } from "react-native-paper";
import { connect } from "react-redux";
import { mutate } from "swr";
import RouteNames from "../../../navigation/route_names";
import CustomAppBar from "../../../utils/components/appbar";
import { GlobalProps, mapStateToProps } from "../../../utils/props";
import LandingSkeleton from "../components/landing_skeleton";
import UserList from "../components/user_list";
import { useUserList } from "../presenter/landing_presenter";

const wait = (timeout: number) => {
  return new Promise(resolve => setTimeout(resolve, timeout));
}

const LandingPage: React.FC<GlobalProps> = (props) => {
  const { auth } = props.authReducer;
  const [refreshing, setRefreshing] = React.useState(false);
  const { users, isError, isLoading, mutate } = useUserList(auth?.access_token ?? "");

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);
    mutate().then((response) => setRefreshing(false)).catch((error) => setRefreshing(false))
  }, []);


  return <>
    <SafeAreaView style={{ display: "flex", flex: 1, flexGrow: 1 }}>
      <CustomAppBar title="Landing" />
      <View style={{ flex: 1 }}>
        {!isLoading ? <ScrollView
          contentContainerStyle={{ marginTop: 16 }}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh} />
          }>
          {
            users?.result.map((item, index) => {
              return <TouchableOpacity key={index.toString()} onPress={() => props.navigation.navigate(RouteNames.LANDING_POSTS, { user_id: item.id })}>
                <View style={{ marginBottom: 16 }}>
                  <Card style={{ paddingHorizontal: 16, paddingVertical: 8 }}>
                    <View style={{ display: "flex", flexDirection: "row", alignItems: "center" }}>
                      <Avatar.Image size={68} source={{ uri: item.profile_picture }} style={{ marginRight: 16 }} />
                      <View style={{ display: "flex", flexDirection: "column" }}>
                        <Text style={{ marginBottom: 4, fontSize: 16, fontWeight: '500' }}>{`${item.name}`}</Text>
                        <Text style={{ marginBottom: 4, fontSize: 14, fontWeight: '400' }}>{item.username}</Text>
                        <Text style={{ marginBottom: 4, fontSize: 14, fontWeight: '400' }}>{item.email}</Text>
                      </View>
                    </View>
                  </Card>
                </View>
              </TouchableOpacity>
            })
          }
        </ScrollView> :
          <FlatList
            data={[0, 0, 0, 0, 0, 0]}
            keyExtractor={(item, index) => index.toString()}
            renderItem={(item) => <LandingSkeleton />}
          />
        }
      </View>
    </SafeAreaView>
  </>
}

export default connect(mapStateToProps, {})(LandingPage);