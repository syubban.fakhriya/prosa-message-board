const RouteNames = {
  LOGIN: "login",
  REGISTER: "register",
  HOME: "home",
  LANDING: "landing",
  LANDING_POSTS: "landing_posts",
  PROFILE: "profile",
  MAIN_MENU: "main_menu",
  POST_DETAIL: "post_detail",
}

export default RouteNames;