import LoginPage from "../feature/auth/pages/login_page";
import RegisterPage from "../feature/auth/pages/register_page";
import HomePage from "../feature/home/pages/home_page";
import PostDetailPage from "../feature/home/pages/post_detail_page";
import LandingPage from "../feature/landing/pages/landing_page";
import LandingPost from "../feature/landing/pages/landing_post";
import MainMenu from "../feature/menu/main_menu";
import ProfilePage from "../feature/profile/pages/profile_page";
import RouteNames from "./route_names";

const Routes = {
  LOGIN: {
    name: RouteNames.LOGIN,
    header: "Login",
    component: LoginPage,
    isHeaderShown: false
  },
  REGISTER: {
    name: RouteNames.REGISTER,
    header: "Register",
    component: RegisterPage,
    isHeaderShown: false
  },
  HOME: {
    name: RouteNames.HOME,
    header: "Home",
    component: HomePage,
    isHeaderShown: false,
  },
  LANDING: {
    name: RouteNames.LANDING,
    header: "Landing",
    component: LandingPage,
    isHeaderShown: false,
  },
  LANDING_POSTS: {
    name: RouteNames.LANDING_POSTS,
    header: "Landing Posts",
    component: LandingPost,
    isHeaderShown: false,
  },
  PROFILE: {
    name: RouteNames.PROFILE,
    header: "Profile",
    component: ProfilePage,
    isHeaderShown: false,
  },
  MAIN_MENU: {
    name: RouteNames.MAIN_MENU,
    header: "Main Menu",
    component: MainMenu,
    isHeaderShown: false,
  },
  POST_DETAIL: {
    name: RouteNames.POST_DETAIL,
    header: "Post Detail",
    component: PostDetailPage,
    isHeaderShown: false,
  }
};

const MainMenuRoutes = {
  HOME: {
    name: RouteNames.HOME,
    header: "Home",
    component: HomePage,
    isHeaderShown: true,
  },
  LANDING: {
    name: RouteNames.LANDING,
    header: "Landing",
    component: LandingPage,
    isHeaderShown: true,
  },
  PROFILE: {
    name: RouteNames.PROFILE,
    header: "Profile",
    component: ProfilePage,
    isHeaderShown: true,
  },
}

export { Routes, MainMenuRoutes };