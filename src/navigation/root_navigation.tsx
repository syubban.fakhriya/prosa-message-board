import { DefaultTheme, NavigationContainer, StackActionHelpers } from "@react-navigation/native"
import { createStackNavigator } from "@react-navigation/stack";
import { SafeAreaProvider } from "react-native-safe-area-context"
import { connect } from "react-redux";
import { GlobalProps, mapStateToProps } from "../utils/props";
import { Routes } from "./routes"
import RouteNames from "./route_names";

const Stack = createStackNavigator();

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white'
  },
};

const RootNavigation = (props?: GlobalProps) => {

  const generateScreens = () => {
    const screens: any[] = [];
    let i = 0;
    for (let screenName in Routes) {
      // @ts-ignore
      const screen = Routes[screenName];
      screens.push(<Stack.Screen
        key={i++}
        name={screen.name}
        component={screen.component}
        options={{
          headerShown: screen.isHeaderShown,
        }}
      ></Stack.Screen>);
    }

    return screens;
  }

  return <SafeAreaProvider>
    <NavigationContainer theme={theme}>
      <Stack.Navigator initialRouteName={props?.authReducer.auth?.access_token != null ? RouteNames.MAIN_MENU : RouteNames.LOGIN}>
        {generateScreens()}
      </Stack.Navigator>
    </NavigationContainer>
  </SafeAreaProvider>
}

export default connect(mapStateToProps)(RootNavigation);