export interface BaseResponse<T> {
  result: T;
  message: string,
  status_code: string,
}

export interface Info {
  seed: string;
  results: number;
  page: number;
  version: string;
}
