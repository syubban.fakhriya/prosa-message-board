export const CustomColors = {
  primary: '#00CEC9',
  space_cadet: '#1A374D',
  ming: '#406882',
  crystal_blue: '#6998AB',
  light_blue: '#B1D0E0',
  water: '#E8F6FC',

  // Grey
  gainsboro: '#DCDCDC',
};