const BASE_URL = "http://localhost:8000"
const LOADING = "Loading"
const SUCCESS = "Success"
const ERROR = "Error"

export { BASE_URL, LOADING, SUCCESS, ERROR }