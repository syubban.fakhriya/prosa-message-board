import { RouteProp } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { bindActionCreators, Dispatch } from "redux";
import { ApplicationState } from "../redux/reducers";

interface AppNavigationProps {
  navigation: StackNavigationProp<any>;
  route: RouteProp<any, any> & { params: any }
}

export const mapStateToProps = (state: ApplicationState) => ({
  // Posts
  homeReducer: state.homeReducer,
  landingReducer: state.landingReducer,
  profileReducer: state.profileReducer,
  authReducer: state.authReducer,
});

export type GlobalProps = AppNavigationProps & ReturnType<typeof mapStateToProps>;