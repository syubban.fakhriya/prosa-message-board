import { Appbar } from "react-native-paper";
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { CustomColors } from "../colors";
import { ReactNode } from "react";
import { Text } from 'react-native';

interface state {
  title: string,
  actions?: ReactNode[];
  enableBack?: boolean;
  backAction?: Function;
}

const CustomAppBar = ({ title, actions, enableBack, backAction }: state): React.ReactElement => {
  return <Appbar style={{
    backgroundColor: 'white',
    zIndex: 90
  }}>
    {(enableBack) ? <Appbar.BackAction onPress={() => backAction != null ? backAction() : () => { }} /> : <></>}
    <Appbar.Content title={title} titleStyle={{ fontWeight: '500', color: CustomColors.space_cadet }} />
    {actions}
  </Appbar>;
}

export default CustomAppBar;