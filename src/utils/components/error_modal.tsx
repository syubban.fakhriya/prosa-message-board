import React from "react";
import { Modal, Portal, Provider } from "react-native-paper";
import { Text, View } from "react-native";
import { AntDesign } from '@expo/vector-icons';

interface state {
  visible: boolean;
  message: string;
  onDismiss: (isVisible: boolean) => void;
}

const ErrorModal = ({ visible, onDismiss, message }: state): React.ReactElement => {
  const [expanded, setExpanded] = React.useState(true);

  const handlePress = () => setExpanded(!expanded);

  const containerStyle = {
    backgroundColor: 'white',
    borderRadius: 16
  };

  return <Portal>
    <Modal
      visible={visible}
      onDismiss={() => onDismiss(false)}
      contentContainerStyle={containerStyle}
      style={{
        margin: 24,
        zIndex: 99,
      }}>
      <View style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        marginVertical: 40,
      }}>
        <AntDesign name="closecircleo" size={40} color={'red'} />
        <Text style={{
          marginTop: 20,
          fontSize: 18,
          fontWeight: '500',
        }}>{"Terjadi Kesalahan"}</Text>
        {message ? <Text style={{
          marginTop: 10,
          fontSize: 14,
        }}>{message}</Text> : <></>}
      </View>
    </Modal>
  </Portal>
}

export default ErrorModal;