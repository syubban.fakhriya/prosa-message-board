import { StatusBar } from 'expo-status-bar';
import { AppState, AppStateStatus, StyleSheet, Text, View } from 'react-native';
import RootNavigation from './src/navigation/root_navigation';
import { DefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import { Provider as Storeprovider } from 'react-redux';
import { store, persistor } from './src/redux/store';
import { SWRConfig } from 'swr';
import { PersistGate } from 'redux-persist/integration/react';

const theme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#00CEC9',
  }
}

export default function App() {
  return (
    <SWRConfig value={{

    }}>
      <Storeprovider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <PaperProvider theme={theme}>
            <RootNavigation />
          </PaperProvider >
        </PersistGate>
      </Storeprovider>
    </SWRConfig>
  );
}
